// Fill out your copyright notice in the Description page of Project Settings.


#include "MyLibrary.h"

// Sets default values
AMyLibrary::AMyLibrary()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AMyLibrary::RecreateComponentPhysicsState(UActorComponent* SkeletalMeshComponent)
{
	if (SkeletalMeshComponent != nullptr && SkeletalMeshComponent->IsValidLowLevel())
	{
		SkeletalMeshComponent->RecreatePhysicsState();
		UE_LOG(LogTemp, Warning, TEXT("DONE"));
	}
}

// Called when the game starts or when spawned
void AMyLibrary::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyLibrary::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

