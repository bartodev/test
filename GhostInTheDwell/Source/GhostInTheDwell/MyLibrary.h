// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyLibrary.generated.h"

UCLASS()
class GHOSTINTHEDWELL_API AMyLibrary : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyLibrary();

	UFUNCTION(BlueprintCallable, Category = "MyLibrary|Physics")
	static void RecreateComponentPhysicsState(class UActorComponent* SkeletalMeshComponent);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
